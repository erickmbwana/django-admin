## The Todos Project

This is the codebase for the Mastering the Django admin video course.

The course can be accessed [on Udemy](https://www.udemy.com/mastering-the-django-admin-dashboard).

### Installation and set up
Make sure you have Python 3 installed in your system. If not, check this link for instructions on how to do it.
Clone the project: `git clone https://gitlab.com/erickmbwana/django-admin.git blog`

Cd into the project: `cd blog`

Install: `pip install -r requirements.txt`

Run the migrations: `python manage.py migrate`

Run the developer server: `python manage.py runserver`

Open your browser on `localhost:8000` to access the running site.

Remember to visit [Lab of coding](https://labofcoding.com). for the latest videos and blog posts on Python, Django, Flask and related stuff.

Happy coding!
