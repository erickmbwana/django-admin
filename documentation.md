## Mastering the Django admin dashboard
One of the most powerful components of Django is the automatic admin interface. It provides a quick, database centric interface that authenticated users can use to manage content on a Django site.
This [course](https://www.udemy.com/mastering-the-django-admin-dashboard) provides a step by step thorough guide towards mastering the admin dashboard.

### The ModelAdmin class
A lot of the built in magic that is provided by Django is configurable using the `ModelAdmin` class.

In most cases, each model will be registered using its own model admin class, in which all the desired behavior of how the model is to be accessed in the admin is specified.

The [Django](https://docs.djangoproject.com/en/1.11/ref/contrib/admin/#modeladmin-options) has an excellent explanation on how to use the `ModelAdmin` class and customize its options.

### Users and the admin dashboard

#### Non staff users
- `is_staff=False` and `is_superuser=False`
- Cannot even access or login into the admin

#### Staff users
- `is_staff=True` and `is_superuser=False`
- Can log in but cannot do anything until they're given permission

#### Superusers
- `is_staff=True` and `is_superuser=True`
- Have all powers, can do anything even, even deleting themselves!

#### Default permissions
- Django comes with a simple permissions system. It provides a way to assign permissions to specific users and groups of users.
- add, change and delete permissions are created for each Django model defined in one of your installed applications.
- Can create custom permissions

### Third party apps used in the project

- django-admin-list-filter-dropdown 1.0.2(https://github.com/mrts/django-admin-list-filter-dropdown)    
- django-admin-rangefilter          0.3.9(https://github.com/silentsokolov/django-admin-rangefilter)     
- django-geojson                    2.11.0(https://github.com/makinacorpus/django-geojson)    
- django-grappelli                  2.12.1(https://github.com/sehmaschine/django-grappelli)    
- django-import-export              1.1.0(https://github.com/django-import-export/django-import-export)     
- django-leaflet                    0.24.0(https://github.com/makinacorpus/django-leaflet)    
- django-summernote                 0.8.10.0(https://github.com/summernote/django-summernote)  

### Tips on how to secure the admin
- Not for the end users, for a few client admins
- Give the most minimal permissions
- Serve under HTTPS
- Use two factor authentication(consider using Django-otp - https://bitbucket.org/psagers/django-otp)
- Limit client access to the admin based on client ip address
